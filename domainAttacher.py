#Script which provide functionality to import domains to remote mongodb
from config import *

domainsList = []

if __name__ == '__main__':
    domainsFileName = input("[*] Path to bugbounty domains to monitor: ").strip()
    bugBountyProgramName = input("[*] BugBounty Program Name: ").strip()
    programType = input("[*] Public or Private: ")
    fo = open(domainsFileName,'r')
    for line in fo:
        line = line.strip()
        if not line: continue
        domainsList.append(line)
    fo.close()
    mongoConnection = mongoConnect()
    monitoringDomainsCollection = mongoCollection(mongoConnection,'dmsystemdb','bountyprogramms')
    if monitoringDomainsCollection.find_one({"programName": bugBountyProgramName}):
        print("[*] Programm already exists. Terminating")
        mongoConnection.close()
        exit()
    monitoringDomainsCollection.save({
                                        'programName': bugBountyProgramName,
                                        'programType': programType,
                                        'monDomains': domainsList
                                    })
    print("[*] Uploaded")
    mongoConnection.close()