#Main Script for security checking
from modules.dnsrecon.dnsdiscovery import moduleDnsDiscovery
from config import *
import os

#Словарь с путями к временным файлам модулей
configPaths = returnConfigsPaths()

if __name__ == '__main__':

    mongoConnection = mongoConnect()
    monitoringDomainsCollection = mongoCollection(mongoConnection,'dmsystemdb','bountyprogramms')
    dataProgramsCollections = getCollectionData(monitoringDomainsCollection,'', True)

    for collection in dataProgramsCollections:
        discoveredDomainsList = []
        programDataDict = {                                                            
            "programName": collection['programName'],
            "programType": collection['programType'],
            "monDomains": collection['monDomains']
        }
    
        for domain in programDataDict['monDomains']:
            discoveredDomainsList.extend(moduleDnsDiscovery(
                domain,
                configPaths['dnsDiscovery']['tempDirPath'],
                configPaths['dnsDiscovery']['rawDictUrl'],
                configPaths['dnsDiscovery']['dictName'],
                configPaths['dnsDiscovery']['dockerImageName'],
                configPaths['dnsDiscovery']['subfinderConfigPath']
            ))
            #print(discoveredDomainsList)

        scanDomainsCollection = mongoCollection(mongoConnection,'dmsystemdb','programdomains')
        dataScannedProgramData = getCollectionData(scanDomainsCollection,{"programName": programDataDict['programName']},False)

        if dataScannedProgramData:
            previousScannedDomainsList = dataScannedProgramData[0]['scannedDomains']
            diffDomainsResultList = list(set(discoveredDomainsList) - set(previousScannedDomainsList))
            if diffDomainsResultList:
                scanDomainsCollection.update_one(
                    {'_id': dataScannedProgramData[0]['_id']},
                    {'$set': 
                        {
                        'programName': programDataDict['programName'],
                        'scannedDomains': discoveredDomainsList
                }})
                
                for diffedDomain in diffDomainsResultList:
                    print("here will be module")
        else:
            scanDomainsCollection.insert_one({
                'programName': programDataDict['programName'],
                'scannedDomains': discoveredDomainsList
            })
                   
    mongoConnection.close()