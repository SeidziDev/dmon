#Config file for database
import pymongo

def returnConfigsPaths():
    return {
                "dnsDiscovery": {
                                'tempDirPath': '/home/Tools/SeidziDCS/modules/dnsrecon/discoverdns_tmp/',
                                'rawDictUrl': 'https://raw.githubusercontent.com/danielmiessler/SecLists/master/Discovery/DNS/subdomains-top1mil-110000.txt',
                                'subfinderConfigPath': '/home/Tools/SeidziDCS/modules/toolsconfig/config.json',
                                'dictName': 'subdomains-top1mil-110000.txt',
                                'dockerImageName': 'subfinder'
                }
    }
DB_USER = ''
DB_PASSWORD = ''

def mongoConnect():
    return pymongo.MongoClient("mongodb+srv://" + DB_USER + ":" + DB_PASSWORD + "@dmsystem-zpkc4.mongodb.net/test?retryWrites=true")

def mongoCollection(connection,databaseName,collectionName):
    db = connection[databaseName]
    return db[collectionName]

def getCollectionData(collection,filter,getAll=True):
    collectionDataList = []
    if getAll:
        for post in collection.find():
            collectionDataList.append(post)
        return collectionDataList
    for post in collection.find(filter):
        collectionDataList.append(post)
    return collectionDataList

