import os

"""
DnsDiscoveryModule(17.03.19) by Seidzi<seidzi@wearehackerone.com>
Require:
        subfinder Docker Container(You should build it from subfinder official Dockerfile with name: subfinder)
        wget(apt-get install wget)

Params: @domain - target domain
        @tempDirPath - temp path for files using by subfinder tool
        @dictUrl - dictionary url in raw format
        @dictName - dict name from dictUrl param
        @toolConfigPath - path to config.json with your API keys(subfinder use this file)
Module uses subfinder to bruteforce, OSINTDiscover and resolve bugbounty programm subdomains
"""

def moduleDnsDiscovery(domain,tempDirPath,dictUrl,dictName,dockerContainerName,toolConfigPath):
    discoveredDomainsList = []
    resultFileName = domain + ".txt"
    os.system("mkdir " + tempDirPath)
    os.system("wget " + dictUrl + " -P " + tempDirPath)
    os.system("cp " + toolConfigPath + " " + tempDirPath)
    os.system("docker run --rm -v \"" + tempDirPath + ":/root/.config/subfinder\" -it " + dockerContainerName + " -d " + domain + " -b -t 100 -w /root/.config/subfinder/" + dictName + " -o /root/.config/subfinder/" + resultFileName + " -nW -v --exclude-sources passivetotal,securitytrails")
    for line in open(tempDirPath + resultFileName,'r'):
        discoveredDomainsList.append(line.strip())
    os.system("rm -R " + tempDirPath)
    return discoveredDomainsList